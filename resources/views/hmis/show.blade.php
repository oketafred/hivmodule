@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('success'))
    <div class="alert alert-danger">{{ session('success') }}</div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="float-left">Show Details</div>
        </div>
        <div class="card-body">
            <p></p>
        </div>
    </div>
</div>
@endsection