@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="float-left">HMIS 096A Health Unit TB Form</div>
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('hmis.index') }}">Back to Record List</a>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('hmis.store') }}" method="POST">
                @csrf
                <div class="my-2">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="unit_tb">Unit TB Number</label>
                            <input name="unit_tb_number" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="hsb_tb_no">HSB TB No</label>
                            <input name="hsb_tb_number" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="district_tb_number">District TB Number</label>
                            <input name="district_tb_number" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="my-2">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="contact_phone_number">Contact Phone Number</label>
                            <input name="contact_phone_number" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="relationship_of_contact_person">Relationship of Contact Person</label>
                            <input name="relationship_of_contact_person" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="patient_status">Is patient a Health Worker</label>
                            <div class="mt-2">
                                <p class="float-left"><input name="is_patient_health_worker" type="radio"> Yes</p>
                                <p class="float-right"><input name="is_patient_health_worker" type="radio"> No</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="my-2">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="cadre_of_health_worker">Cadre of Health Worker</label>
                            <input name="cadre_of_health_worker" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="date_treatment_started">Date Treatment Started</label>
                            <input name="date_treatment_started" type="date" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="regime">Regime</label>
                            <input name="regime" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="my-2">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="type_of_patient">Type of Patient</label>
                            <select name="type_pf_patient" class="form-control">
                                <option value="New">New</option>
                                <option value="Old">Old</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="specify_regimen">Specify Regimen</label>
                            <input name="specific_regimen" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="transfer_in_from">Transfer in from</label>
                            <input name="transfer_in_form" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="my-4">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-secondary">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection