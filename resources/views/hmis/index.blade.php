@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('success'))
    <div class="alert alert-danger">{{ session('success') }}</div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="float-left">HMIS 096A Health Unit TB Form</div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('hmis.create') }}">Add Record</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Unit TB Number</th>
                        <th>HSD TB Number</th>
                        <th>District TB Number</th>
                        <th>Contact Phone</th>
                        <th>Action</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($hmis_records as $hmis)
                    <tr>
                        <td>{{ $hmis->id }}</td>
                        <td>{{ $hmis->unit_tb_number }}</td>
                        <td>{{ $hmis->hsb_tb_number }}</td>
                        <td>{{ $hmis->district_tb_number }}</td>
                        <td>{{ $hmis->contact_phone_number }}</td>
                        <td><a class="btn btn-primary btn-sm" href="{{ route('hmis.show', $hmis->id) }}">Show Detail</a>
                        </td>
                        <td><a class="btn btn-danger btn-sm" href="{{ route('hmis.destroy', $hmis->id) }}">Delete</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td>No Record Found</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection