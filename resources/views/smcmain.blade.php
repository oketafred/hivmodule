@extends('layouts.app')

@section('main')
@include('errrors')
<div class="container">
    <form action="/malecircumsion2" method="post">
    {{csrf_field()}}
    @section('editMethod') 
        @show
                <div class="ai-page-frame">
                        <div class="headers" style="text-align: center;">
                            <h3 >Safe Male Circumcision Client Card</h3>
                        </div>
                        <section>
                                                <h3>Facility Information</h3>
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        
                                        <td   class="form-inline">
                                            <label>Site Type:</label>
                                                    <select class="form-control" name="st">
                                                    <option selected>Choose..</option>
                                                    <option value="Static/Fixed " >Static/Fixed</option>
                                                    <option value="Outreach" >Outreach</option>
                                                    <option value="Camp" >Camp</option>
                                                    
                                                </select>
                                                &nbsp
                                        </td>
                                    
                                        <td>
                                            <label>National ID:</label>
                                            <input type="text"  name="ID" value="@yield('A')"/>
                                                        &nbsp
                                        </td>
                                        <td>
                                            <label>Serial ID:</label>
                                            <input type="text" name="serid" value="@yield('ser')"/>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                        </section>
                        <section>
                            <h3>B: Client Information</h3>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                        <td>
                                            <label>How did you hear about SMC</label>
                                        </td>
                                        <td>
                                                <input type="checkbox" name="hw_u_heard_smc[]" value="Friend/Family"/>
                                                <label>Friend/Family</label>
                                                <input type="checkbox" name="hw_u_heard_smc[]" value="Radio"/>
                                                <label>Radio</label>
                                                <input type="checkbox" name="hw_u_heard_smc[]"  value="TV"/>
                                                <label>TV</label>
                                                <input type="checkbox" name="hw_u_heard_smc[]"  value="HCWReferral"/>
                                                <label>HCW Referral</label>
                                                <input type="checkbox" name="hw_u_heard_smc[]" value="VHT"/>
                                                <label>VHT</label>
                                                <input type="checkbox"  name="hw_u_heard_smc[]"  value="Megaphones/CommunityRadio"/>
                                                <label>Megaphones/Community Radio</label>

                                            <div style="width: 100%">
                                                <input type="checkbox" name="hw_u_heard_smc[]"  value="Other"/>
                                                <label>Other</label>

                                                    <span id="99613">
                                                    <label>Other Specify</label>
                                                    <input type="text" name="hw_u_heard_smc[]"  />
                                                    </span>
                                            </div>
                                        </td>
                                   
                                </tr>
                                <tr>
                                
                                        <td>
                                            <label>Care Entry Point</label>
                                        </td>
                                        <td>

                                            
                                            {!! Form::radio('entry', 'SMC', isset($smcmain) ? $smcmain->careentrypoint === 'SMC': false); !!}
                                            <label>SMC</label>
                                           
                                            {!! Form::radio('entry', 'HCT', isset($smcmain) ? $smcmain->careentrypoint === 'HCT' : false); !!}
                                            <label>HCT</label>
                            
                                            
                                            
                                        </td>
                                   
                                </tr>
                                </tbody>
                            </table>
                        </section>
                                <section>
                                    <h3>
                                        <b>C: Client Medical History</b>
                                    </h3>
                                    <h4>C1: Knowledge of HIV Status</h4>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <div >

                                            <tr>
                                                <td>
                                                    <label>Sexually Active?</label>
                                                    {!! Form::radio('sexual', 'Yes', isset($smcmain) ? $smcmain->sexuallyactive  === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('sexual', 'No', isset($smcmain) ? $smcmain->sexuallyactive === 'No' : false); !!}
                                                    <label>No</label>
                                                </td>
                                                <td colspan="2">
                                                    <label>Client tested for HIV in the past 4 weeks?</label>
                                                    {!! Form::radio('tested', 'Yes', isset($smcmain) ? $smcmain->testbefore === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('tested', 'No', isset($smcmain) ? $smcmain->testbefore === 'No' : false); !!}
                                                    <label>No</label>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <label>HCT offered to the Client?</label>
                                                    {!! Form::radio('hct', 'Yes', isset($smcmain) ? $smcmain->HTCoffered=== 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('hct', 'No', isset($smcmain) ? $smcmain->HTCoffered === 'No' : false); !!}
                                                    <label>No</label>
                                                    
                                                </td>
                                                <td colspan="2">
                                                    <label>Client tested for HIV during this appointment?</label>
                                                    {!! Form::radio('during', 'Yes', isset($smcmain) ? $smcmain->testnow === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('during', 'No', isset($smcmain) ? $smcmain->testnow === 'No' : false); !!}
                                                    <label>No</label>
                                                   
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="3">

                                                    <span id="smc_client_test_result_container">
                                                    <label>Client HIV test results:</label>  
                                                    {!! Form::radio('sexual1', 'Positive', isset($smcmain) ? $smcmain->testresults === 'Positive' : false); !!}
                                                    <label>Positive</label>
                                                    {!! Form::radio('sexual1', 'Negative', isset($smcmain) ? $smcmain->testresults=== 'Negative' : false); !!}
                                                    <label>Negative</label>
                                                
                                                    </span>

                                                    <span id="162900">
                                                    <label>If HIV+ In care?</label>
                                                    {!! Form::radio('care', 'Yes', isset($smcmain) ? $smcmain->incare === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('care', 'No', isset($smcmain) ? $smcmain->incare === 'No' : false); !!}
                                                    <label>No</label>
                                                    
                                                </span>

                                                    <span id="162901">
                                                    <label>Referred?</label>
                                                    {!! Form::radio('referred', 'Yes', isset($smcmain) ? $smcmain->reffered=== 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('referred', 'No', isset($smcmain) ? $smcmain->reffered=== 'No' : false); !!}
                                                    <label>No</label>
                                                   
                                                        </span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Tetanus vaccination</label></td>
                                                <td>
                                                    <label>Date of TT1</label>
                                                    <input type="date" default="today" name="TT1" value="@yield('t1')"/>
                                                </td>
                                                <td>
                                                    <label>Date of TT2</label>
                                                    <input type="date" default="tod" name="TT2" value="@yield('t2')"/>
                                                    
                                                </td>
                                            </tr>
                                        </div>

                                        </tbody>
                                    </table>
                        </section>
                    <section>
                            <h4>C2: Medical History</h4>
                            <table class="table table-bordered">
                                <tbody>
                                <div>

                                    <tr>
                                        <td>
                                            <label>Bleeding disorder?</label>
                                                    {!! Form::radio('disorder', 'Yes', isset($smcmain) ? $smcmain->bleeding === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('disorder', 'No', isset($smcmain) ? $smcmain->bleeding === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                        <td>
                                            <label>Genital ulcers?</label>
                                                    {!! Form::radio('ulcers', 'Yes', isset($smcmain) ? $smcmain->ulcers === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('ulcers', 'No', isset($smcmain) ? $smcmain->ulcers === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Urethral discharge?</label>
                                                    {!! Form::radio('discharge', 'Yes', isset($smcmain) ? $smcmain->urethral === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('discharge', 'No', isset($smcmain) ? $smcmain->urethral === 'No' : false); !!}
                                                    <label>No</label>
                                           
                                        </td>
                                        <td>
                                            <label>Penile warts?</label>
                                                    {!! Form::radio('warts', 'Yes', isset($smcmain) ? $smcmain->penile_wts === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('warts', 'No', isset($smcmain) ? $smcmain->penile_wts === 'No' : false); !!}
                                                    <label>No</label>
                                           
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Pain on Urination?</label>
                                                    {!! Form::radio('pain', 'Yes', isset($smcmain) ? $smcmain->pain_on_urn === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('pain', 'No', isset($smcmain) ? $smcmain->pain_on_urn === 'No' : false); !!}
                                                    <label>No</label>
                                           
                                        </td>
                                        <td>
                                            <label>Difficult in retracting foreskin?</label>
                                                    {!! Form::radio('foreskin', 'Yes', isset($smcmain) ? $smcmain->diff_in_foreskin=== 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('foreskin', 'No', isset($smcmain) ? $smcmain->diff_in_foreskin === 'No' : false); !!}
                                                    <label>No</label>
                                           
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Swelling on scrotum?</label>
                                                    {!! Form::radio('scrotum', 'Yes', isset($smcmain) ? $smcmain->swelling_scrotum === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('scrotum', 'No', isset($smcmain) ? $smcmain->swelling_scrotum=== 'No' : false); !!}
                                                    <label>No</label>
                                           
                                        </td>
                                        <td>
                                            <label>Electile dysfunction?</label>
                                                     {!! Form::radio('dysfunction', 'Yes', isset($smcmain) ? $smcmain->elec_dysfunction === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('dysfunction', 'No', isset($smcmain) ? $smcmain->elec_dysfunction=== 'No' : false); !!}
                                                    <label>No</label>
                                           
                                        </td>
                                    </tr>

                                    <tr> 
                                        <td colspan="2">
                                        <label>Other Specify:</label>
                                        <input  type="text" name="spec3" value="@yield('tt')"/>
                                        </td>
                                    </tr>

                                </div>
                                </tbody>
                            </table>
                        </section>
                        <section>
                            <h4>C3: Client Undergoing Treatment</h4>
                            <table class="table table-bordered">
                                <tbody>
                                <div>

                                    <tr>
                                        <td>
                                            <label>Hypertension?</label>
                                                    {!! Form::radio('hypertension', 'Yes', isset($smcmain) ? $smcmain->hypertension === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('hypertension', 'No', isset($smcmain) ? $smcmain->hypertension=== 'No' : false); !!}
                                                    <label>No</label>
                                           
                            
                                        </td>
                                        <td>
                                            <label>Anaemia?</label>
                                                    {!! Form::radio('anaemia', 'Yes', isset($smcmain) ? $smcmain->anaemia === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('anaemia', 'No', isset($smcmain) ? $smcmain->anaemia=== 'No' : false); !!}
                                                    <label>No</label>
                                           
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Diabetes?</label>
                                                    {!! Form::radio('diabetes', 'Yes', isset($smcmain) ? $smcmain->diabetes === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('diabetes', 'No', isset($smcmain) ? $smcmain->diabetes === 'No' : false); !!}
                                                    <label>No</label>
                                           
                                        </td>
                                        <td>
                                            <label>HIV/AIDS?</label>
                                                    {!! Form::radio('hiv', 'Yes', isset($smcmain) ? $smcmain->HIV_AIDS === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('hiv', 'No', isset($smcmain) ? $smcmain->HIV_AIDS === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                        <label>Other (Specify):</label>
                                        <input  type="text" name="spec" value="@yield('os')"/>
                                           
                                        </td>
                                    </tr>

                                </div>
                                </tbody>
                            </table>
                        </section>
                        <section>
                            <h4>C4:Allegies</h4>
                            <table class="table table-bordered">
                                <tbody>
                                <div >

                                    <tr>
                                        <td>
                                            <label>Local Anesthetics?</label>
                                                    {!! Form::radio('local', 'Yes', isset($smcmain) ? $smcmain->local_anesthetics === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('local', 'No', isset($smcmain) ? $smcmain->local_anesthetics === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                        <td>
                                            <label>Antiseptics?</label>
                                                    {!! Form::radio('antiseptics', 'Yes', isset($smcmain) ? $smcmain->Antiseptics === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('antiseptics', 'No', isset($smcmain) ? $smcmain->Antiseptics === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <label>Any other medication (Specify):</label>
                                        <input  type="text" name="spec1" value="@yield('spec1')" />
                                            
                                        </td>
                                    </tr>
                                </div>
                                </tbody>
                            </table>
                        </section>
                        <section>
                            <h4>C5: Physical Exam</h4>
                            <table class="table table-bordered">
                                <tbody>
                                <div>
                                    <tr>
                                        <td style="width: 40%">
                                            BP:<br/>
                                            <p class="left">
                                            <label>Systolic</label>
                                            <input  type="text" name="Systolic" value="@yield('sy')"/>  
                                            </p>
                                            <p class="left" style="margin-left: 5px; margin-right: 5px;">
                                                /
                                            </p>
                                            <p class="left">
                                            <label>Diastolic</label>
                                            <input  type="text" name="diastolic" value="@yield('dia')"/>  
                                            </p>
                                        </td>
                                        <td>
                                            <label>Pulse(rate/min):</label>
                                            <input  type="text" size='8' name="pulse" value="@yield('min')"/>  
                                            
                                        </td>
                                        <td >
                                            <label>Temp(&#176;C):</label>
                                            <input  type="text" size='8' name="temp" value="@yield('temp')"/>
                                        </td>
                                        <td>
                                            <label>RR:</label>
                                            <input  type="text" size='8' name="rr" value="@yield('r1')"/>
                                        </td>
                                        <td>
                                            <label>Weight(Kgs):</label>
                                            <input  type="text" size='8' name="weight" value="@yield('wt')"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Urethra Discharge?</label>
                                                    {!! Form::radio('disc', 'Yes', isset($smcmain) ? $smcmain->urethra_d === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('disc', 'No', isset($smcmain) ? $smcmain->urethra_d === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                        <td colspan="4">
                                            <label>Adhesions?</label>
                                                    {!! Form::radio('adhesions', 'Yes', isset($smcmain) ? $smcmain->Adhesion === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('adhesions', 'No', isset($smcmain) ? $smcmain->Adhesion === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Anatomical abnormalities?</label>
                                                    {!! Form::radio('abnormalities', 'Yes', isset($smcmain) ? $smcmain->Anotomical_abns=== 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('abnormalities', 'No', isset($smcmain) ? $smcmain->Anotomical_abns === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>

                                        <td colspan="4">
                                            <label>GUD?</label>
                                                    {!! Form::radio('GUD', 'Yes', isset($smcmain) ? $smcmain->GUD === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('GUD', 'No', isset($smcmain) ? $smcmain->GUD === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Balanitis?</label>
                                                     {!! Form::radio('Balanitis', 'Yes', isset($smcmain) ? $smcmain->Blanitis === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('Balanitis', 'No', isset($smcmain) ? $smcmain->Blanitis === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                        <td colspan="4">
                                            <label>Genital warts</label>
                                                    {!! Form::radio('Genital', 'Yes', isset($smcmain) ? $smcmain->Genital_wts === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('Genital', 'No', isset($smcmain) ? $smcmain->Genital_wts === 'No' : false); !!}
                                                    <label>No</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Surgical disorders?</label>
                                                     {!! Form::radio('Surg', 'Yes', isset($smcmain) ? $smcmain->Surg_disorders === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('Surg', 'No', isset($smcmain) ? $smcmain->Surg_disorders === 'No' : false); !!}
                                                    <label>No</label>
                            
                                                    
                                        </td>
                                        <td colspan="4">
                                            <label>Other STI/abnormality?</label>
                                                    {!! Form::radio('STI', 'Yes', isset($smcmain) ? $smcmain->STI_abns=== 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('STI', 'No', isset($smcmain) ? $smcmain->STI_abns === 'No' : false); !!}
                                                    <label>No</label>

                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Open Wounds/recent healed scars?</label>
                                                    {!! Form::radio('scars', 'Yes', isset($smcmain) ? $smcmain->openwound === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('scars', 'No', isset($smcmain) ? $smcmain->openwound === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                        <td>
                                            <label>Jiggers?</label>
                                                    {!! Form::radio('Jiggers', 'Yes', isset($smcmain) ? $smcmain->jiggers === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('Jiggers', 'No', isset($smcmain) ? $smcmain->jiggers=== 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                        <td colspan="3">
                                        <label>Other Specify:</label>
                                        <input  type="text" name="spec2" value="@yield('oo')"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <label>TT given during this appointment</label>
                                                     {!! Form::radio('TT', 'Yes', isset($smcmain) ? $smcmain->TTgiven=== 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('TT', 'No', isset($smcmain) ? $smcmain->TTgiven === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>

                                    </tr>
                                </div>
                                </tbody>
                            </table>
                        </section>
                        <section>
                            <h3>Consent</h3>
                            <h4>D: Eligibility for Circumcision</h4>
                            <table class="table table-bordered">
                                <tbody>
                                <div>
                                    <tr>
                                        <td>
                                            <label>Client in good general health?</label>
                                                     {!! Form::radio('good', 'Yes', isset($smcmain) ? $smcmain->goodhealth=== 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('good', 'No', isset($smcmain) ? $smcmain->goodhealth === 'No' : false); !!}
                                                    <label>No</label>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Client counseled about SMC?</label>
                                                    {!! Form::radio('counseled', 'Yes', isset($smcmain) ? $smcmain->cc_smc === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('counseled', 'No', isset($smcmain) ? $smcmain->cc_smc === 'No' : false); !!}
                                                    <label>No</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Informed consent/assent for SMC given?</label>
                                                     {!! Form::radio('assent', 'Yes', isset($smcmain) ? $smcmain->ic_smc === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('assent', 'No', isset($smcmain) ? $smcmain->ic_smc === 'No' : false); !!}
                                                    <label>No</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Client eligible for circumcision after history/physical exam?</label>
                                                    {!! Form::radio('exam', 'Yes', isset($smcmain) ? $smcmain->eligible === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('exam', 'No', isset($smcmain) ? $smcmain->eligible === 'No' : false); !!}
                                                    <label>No</label>
                                        </td>
                                    </tr>
                                </div>
                                </tbody>
                            </table>
                        </section>
                        <section>
                            <h4>E: Circumcision Procedure</h4>
                            <table class="table table-bordered">
                                <tbody>
                                    <div>
                                        <tr>
                                            <td colspan="3">
                                                <label>Date of circumcision:</label>
                                                <input type="date" name="dtt" value="@yield('dc')"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Time started:</label>
                                                <input type="time" name="s_time" value="@yield('tts')">
                                            </td>
                                            <td colspan="2">
                                                <label>End Time:</label>
                                                <input type="time" name="e_time" value="@yield('end')">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label>Type of Anesthesia
                                                    <br/>
                                                    <i>Local:</i>
                                                </label>
                                            </td>
                                            <td colspan="2">
                                            <label>Lignocaine 1&#37;</label>
                                            (mls)
                                            <input  type="text" name="Lign" value="@yield('ln')"/>
                                                <br/>
                                                <label>Bupivicaine 0.25&#37;</label>
                                            (mls)
                                            <input  type="text" name="Bupi" value="@yield('bp')"/>
                                            </td>

                                            Type of circumcision Procedure
                                        </tr>

                                        <tr>
                                            <td rowspan="2">
                                                <label>Type of circumcision Procedure</label>

                                            </td>

                                            <td class="lablex2" rowspan="2" style="width:45%">
                                                     {!! Form::radio('proce', 'DorsalSlit', isset($smcmain) ? $smcmain->typeofcircumcision === 'DorsalSlit' : false); !!}
                                                     <label>Dorsal slit</label>
                                                    {!! Form::radio('proce', 'ForcepsGuided', isset($smcmain) ? $smcmain->typeofcircumcision === 'ForcepsGuided' : false); !!}
                                                    <label>Forceps guided</label>
                                                    {!! Form::radio('proce', 'Sleeve', isset($smcmain) ? $smcmain->typeofcircumcision === 'Sleeve' : false); !!}
                                                    <label>Sleeve</label>
                                           
                                                <br/>
                                                <label>Other Specify:</label>
                                                <input  type="text" name="othersp" value="@yield('ox')" />

                                            </td>
                                            <td class="lablex2" style="width: 20%"> 

                                                <div style="margin-bottom: 14px">
                                                    <label>PrePex</label>
                                                    {!! Form::radio('prep', 'Yes', isset($smcmain) ? $smcmain->prepex === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('prep', 'No', isset($smcmain) ? $smcmain->prepex === 'No' : false); !!}
                                                    <label>No</label>
                                                    
                                                </div>
                                                <div style="width: 100%">
                                                    <label>Size</label>
                                                    <input  type="text" name="siz" value="@yield('vx')" />

                                                </div>
                                                <br/>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="margin-bottom: 14px">
                                                    <label>Shang Ring</label>
                                                    {!! Form::radio('ring', 'Yes', isset($smcmain) ? $smcmain->shangring === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('ring', 'No', isset($smcmain) ? $smcmain->shangring === 'No' : false); !!}
                                                    <label>No</label>
                                                    
                                                </div>
                                                <div style="width: 100%">
                                                    <label>Size</label>
                                                    <input  type="text" name="sizz" value="@yield('vx2')" />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="form-inline">
                                                <label><span class="required">*</span>Name of Circumciser:</label>
                                                <span id="name_of_circumciser">
                                                <select class="form-control" name="nam">
                                                    <option selected>Choose a Provider</option>
                                                    <option value="Banji,Lilian " >Banji,Lilian</option>
                                                    <option value="Ocaye,Samuel" >Ocaye,Samuel</option>
                                                    <option value="Achako,Sereh" >Achako,Sereh</option>
                                                    <option value="Achako,Serah" >Achako,Serah</option>
                                                    <option value="Akello,Molly" >Akello,Molly</option>
                                                    <option value="Dianah Nankunda" >Amego,Henry</option>
                                                    <option value="Dan Kato" >Dan Kato</option>
                                                    <option value="OMunu,Richard" >Munu,Richard</option>
                                                </select>
                                                    </span>
                                            </td>
                                            <td>
                                                    <label>Cadre:</label>
                                                    <input  type="text" name="cadre" value="@yield('cad1')" />
                                            </td>
                                        </tr>
                                        <tr>

                                            <td class="form-inline">
                                                <label><span class="required">*</span>Name of Assistant:</label>
                                                <span id="name_of_assistant">
                                                <select class="form-control" name="assis">
                                                    <option selected>Choose a Provider</option>
                                                    <option value="Banji,Lilian " >Banji,Lilian</option>
                                                    <option value="Ocaye,Samuel" >Ocaye,Samuel</option>
                                                    <option value="Achako,Sereh" >Achako,Sereh</option>
                                                    <option value="Achako,Serah" >Achako,Serah</option>
                                                    <option value="Akello,Molly" >Akello,Molly</option>
                                                    <option value="Dianah Nankunda" >Amego,Henry</option>
                                                    <option value="Dan Kato" >Dan Kato</option>
                                                    <option value="OMunu,Richard" >Munu,Richard</option>
                                                </select>
                                                    </span>
                                            </td>
                                            <td colspan="2">
                                                    <label>Cadre:</label>
                                                    <input  type="text" name="cadre1" value="@yield('cad2')" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <label>Adverse events during procedure?</label>
                                                    {!! Form::radio('events', 'Yes', isset($smcmain) ? $smcmain->events_during_proc === 'Yes' : false); !!}
                                                    <label>Yes</label>
                                                    {!! Form::radio('events', 'No', isset($smcmain) ? $smcmain->events_during_proc === 'No' : false); !!}
                                                    <label>No</label>
                                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <label>
                                                    <i>If yes refer the SMC Adverse Event form</i>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                            <label>Severity:</label>
                                            {!! Form::radio('severity2', 'Moderate', isset($smcmain) ? $smcmain->severit === 'Moderate' : false); !!}
                                            <label>Moderate</label> 
                                            {!! Form::radio('severity2', 'Severe', isset($smcmain) ? $smcmain->severit=== 'Severe' : false); !!} 
                                            <label>Severe</label>  
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                <div>
                                                <label>Treatment given:</label>
                                                    <input  type="text" name="tgiven" value="@yield('tmt')" />
                                                
                                                </div>
                                            </td>
                                        </tr>
                                    </div>
                                    </tbody>
                                </table>
                            </section>
                            <section>
                                <h4>Post Operation Mangement (After 30 minutes)</h4>
                                <table class="table table-bordered">
                                    <tbody>
                                    <div class="input-group mb-3">
                                        <tr>
                                            <td>
                                            BP:<br/>
                                                <p class="left">
                                                <label>Systolic</label>
                                                <input  type="text" name="Systolic2" value="@yield('syst')" />  
                                                </p>
                                                <p class="left" style="margin-left: 5px; margin-right: 5px;">
                                                    /
                                                </p>
                                                <p class="left">
                                                <label>Diastolic</label>
                                                <input  type="text" name="diastolic2" value="@yield('ddd')" />  
                                                </p>
                                            </td>
                                            <td>
                                                    <label>Pulse:</label>
                                                    <input  type="text" name="Puls" value="@yield('pls')" />
                                                
                                            </td>
                                            <td>
                                                    <label>RR:</label>
                                                    <input  type="text" name="rrz" value="@yield('rx')" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                    <label>List Post Operative medication given:</label>
                                                    <input  type="text" name="listpost" value="@yield('pom')" />
                                                
                                            </td>
                                        </tr>
                                    </div>
                                </tbody>
                            </table>
                        </section>
                        <section>
                            <div class="form-group">
                                                                    
                                    <align-left><button type="submit" class="btn btn-primary">
                                                EnterForm
                                    </button></align-left>
                                                            
                            </div>
                        </section>
                </div>
   
    </form>
</div>
@endsection
