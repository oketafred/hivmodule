@extends('layouts.app')

@section('content')
<div class="container-fluid">
<h3>OUT PATIENT DEPARTMENT Patient details</h3>
<fieldset>
    <table width="100%">
    </table>
      <div class="form-row">
        <div class="col-auto">
          {{ Form::label('','Visit date:') }}
        </div>
        <div class="col-1">
          {{ $patients->visit_date }}
        </div>
        <div class="col-4"></div>
        <div class="col-auto">
          {{ Form::label('','Serial number:') }}
        </div>
        <div class="col-3">
          {{ $patients->serial_number }}
        </div>
      </div>
    </table>
  </fieldset>
  <br>
<fieldset> 
<h3>Vitals</h3>
  <table>
    <div class="form-row">
      <div class="col-auto">
        {{ Form::label('','M.U.C.A(cm):') }}
      </div>
      <div class="col-1">
        {{ $patients->mauc }}
      </div> 
      <div class="col-1"></div> 
      <div class="col-auto">
        {{ Form::label('','M.A.U.C:') }}
      </div>
      <div class="col-2">
         {{ $patients->muac_cm }}
      </div>
      <div class="col-2"></div>
      <div class="col-auto">
        {{ Form::label('','Weight:')}}
      </div>
      <div class="col-1">
        {{ $patients->weight }} 
      </div>
      <div class="col-auto">
        {{ Form::label('','Height/length(cm):')}}
      </div>
      <div class="col-1">
        {{ $patients->height }}
      </div>    
    </div>
    <br>
   

    <div class="form-row">
      <div class="col-auto">
        {{ Form::label('','BMI:')}}
      </div>
      <div class="col-1">
        {{ $patients->bmi }} 
      </div>
       
      <div class="col-auto">
        {{ Form::label('','Age of weight Z-score:')}}
      </div>
      <div class="col-1">
        {{ $patients->age_of_weight_zscore }}
      </div>
      <div class="col-2"></div>
      <div class="col-auto">
        {{ Form::label('','Height of age Z-score:')}}
      </div>
      <div>
        {{ $patients->height_of_age_zscore }}
      </div>
    </div>
    <br>
    <div class="form-row">
      <div class="col-2.5">
        {{ Form::label('','Blood pressure-Systolic (mmg):')}}
        {{ $patients->blood_pressure_systolic }}
      </div>
      <div class="col-1"></div>
      <div class="col-2.5">
        {{ Form::label('','Blood pressure_Diastolic (mmg):')}}
        {{ $patients->blood_pressure_diastolic }}
      </div>
      <div class="col-1"></div>
      <div class="auto">
        {{ Form::label('','Blood sugar(mmg):')}}
      </div>
      <div class="col-1">
        {{ $patients->blood_sugar }}
      </div>
      <div>
        {{ Form::label('','Temp(C):'),['class'=>'col-auto']}}
      </div>
      <div class="col-1">
        {{ $patients->temp }}
      </div>
    </div>
  </table>
</fieldset>
<br> 
<fieldset>
    <h3>Follow information</h3>
    <div class="form-row">
    	<div class="col-auto">
          {{ Form::label('','Next of kin:')}}
        </div>
        <div class="col-3">
          {{ $patients->next_of_kin }}
        </div>
        <div class="col-auto">
          {{ Form::label('','Need of palliative care:')}}
        </div>
        <div>
          {{ $patients->palliative_care }}
        </div>
        <div class="col-1"></div>
        <div class="col-auto">
          {{ Form::label('','Patient classification:')}}
        </div>
        <div>
          {{ $patients->patient_classification }}
        </div>
      	</div>
      	<br>
      	<div class="form-row">
        <div class="col-auto">
          {{ Form::label('','Tobacco use:')}}
        </div>
        <div>
          {{ $patients->tobacco}}
        </div>
        <div class="col-3"></div>
        <div class="col-auto">
          {{ Form::label('','Alcohol use:')}}
        </div>
        <div>
          {{ $patients->alcohol}}
        </div>
        
      	</div>
  	</fieldset>
  	<br>
  	<fieldset>
    	<h3>Malaria </h3>
    	<div class="form-row">
      	<div class="col-auto">
        	{{ Form::label('','Fever:')}}
      	</div>
      	<div>
        	{{ $patients->fever}}
      	</div>
      	<div class="col-1"></div>
      	<div class="col-auto">
        	{{ Form::label('','Test done:')}}
      	</div>
      	<div>
        	{{ $patients->test_done}}
      	</div>
      	<div class="col-1"></div>
      	<div class="col-auto">
        	{{ Form::label('','Results:')}}
      	</div>
     	<div>
        	{{ $patients->results}}
      	</div>
  	</fieldset>
  	<br>
    <fieldset>
      <h3>TB</h3>
      <div class="form-row">
      <div class="col-auto">
        {{ Form::label('','New Presumed case:')}}
      </div>
      <div>
        {{ $patients->results_of_new_presumed_case}}
      </div>
      <div class="col-1"></div>
      <div class="col-auto">
        {{ Form::label('','Sent to lab:')}}
      </div>
      <div>
        {{ $patients->sent_to_lab}}
      </div>
      <div class="col-1"></div>
      <div class="col-auto">
        {{ Form::label('','LAB test Results:')}}
      </div>
      <div>
        {{ $patients->lab_test_results}}
      </div>
      <div class="col-1"></div>
      <div class="col-auto">
        {{ Form::label('','Linked to TB:')}}
      </div>
      <div>
        {{ $patients->linked_to_TB }}
      </div>
    </div>
  </fieldset>
  <br>
  <fieldset>
    <h3>New diagnosis</h3>
    <table class="multi" width="100%">
      <tbody>
        <?php $no=1; ?>
       
        <tr>
           <?php $diagnose_array=explode(',', $patients->diagnosis); ?>
           @foreach($diagnose_array as $key=>$value)
          <td style="width: 5%">{{$no++}}</td>
          <td style="width: 70%" class="form-control">
           <?php echo $value; ?>
            
          </td>
          @endforeach
        </tr>
        
      </tbody>
    </table>
  </fieldset>
  <br>
   <fieldset>
    <h3>Drugs</h3>
    <table class="table table-bordered" width="100%" >
      <thead>
        <tr>
          <th >Drug</th>
          <th style="width: 10%">Unit per dose</th>
          <th>Doses per day</th>
          <th>Duration</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php
          $drugs_array=explode(',', $patients->drug);
          $units_per_days_array=explode(',', $patients->units_per_day);
          $time1s=explode(',', $patients->time1);
        ?>
        @for($i=0; $i < count($drugs_array) ; $i++)
          <tr>
            <td>
              {{ $drugs_array[$i] }}
            </td>
            <td>
              {{ $drugs_array[$i] }}
            </td>
            <td>
              {{ $units_per_days_array[$i] }}
            </td>
            <td>
              {{ $time1s[$i] }}
            </td>
            <td>
              {{ $drugs_array[$i] }}
            </td>
          </tr>
        @endfor          
      </tbody>
    </table>
  </fieldset>
  <br>
  <fieldset>
    <h3>Disability</h3>
    <div class="form-row">
      <div class="col-2">
        {{ Form::label('','Disability:')}}
        {{ $patients->disability }}
      </div>
      <div class="col-1"></div>
      <div class="col-3">
        {{ Form::label('','Type of Disability:')}}
        {{ $patients->type_of_disability }}
      </div>
      <div class="col-1"></div>
      <div class="col-2">
        {{ Form::label('Device provided:')}}
        {{ $patients->device_provided }}
      </div>
    </div>
  </fieldset>
  <br>
  <fieldset>
    <h3>Out patient Refferals</h3>
    <div class="form-row">
      <div class="col-auto">
        <label>Referral in number:</label>
      </div>
      <div class="col-2">
        {{ $patients->refferal_in_number }}
      </div>
      <div class="col-4"></div>
      <div class="col-auto">
        <label>Referral out number:</label>
      </div>
      <div class="col-2">
        {{ $patients->refferal_out_number }}
      </div>
    </div>
  </fieldset>
</div>
@endsection()