<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hmis extends Model
{
    protected $fillable = [
        'unit_tb_number',
        'hsb_tb_number',
        'district_tb_number',
        'contact_phone_number',
        'relationship_of_contact_person',
        'is_patient_health_worker',
        'cadre_of_health_worker',
        'date_treatment_started',
        'regime',
        'type_pf_patient',
        'specific_regimen',
        'transfer_in_form',
    ];
}
