<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\smcmain;
use Yajra\Datatables\DataTables;
class MalecircumcisionController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        
        if(request()->ajax())
        {
            return datatables()->of(smcmain::latest()->get())
                ->addColumn('action',function( $smcmain){
                   
                    $button = '<a href="'. route('malecircumsion2.edit', $smcmain->id) .'" class="edit btn btn-primary btn-sm" type="submit">Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button class="btn-delete btn btn-danger" data-remote="/malecircumsion2/'.$smcmain->id.'">Delete</button>';
                    return $button;
               
                })
                ->addColumn('more',function( $smcmain){
                    $button = '<a href="'. route('malecircumsion2.show', $smcmain->id) .'" class="more btn  btn-primary btn-sm" type="submit"><i class="glyphicon glyphicon-trash"></i> More</a>';
                return $button;
               
            })
                ->rawColumns(['action','more'])
                ->make(true);
        }

        return view('smcmaintable');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('smcmain');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
       $this->validate($request,[
            'ID'=>'required',
            'st'=>'required',
            'sexual'=>'required:smcmains',
            'serid'=>'required',
            'tested'=>'required',
            'hct'=>'required', 
            'during'=>'required',
            'TT1'=>'required', 
            'TT2'=>'required',
            'sexual1'=>'required',
            'care'=>'required',
            'referred'=>'required',
            'disorder'=>'required',
            'ulcers'=>'required',
            'discharge'=>'required',
            'warts'=>'required',
            'pain'=>'required',
            'spec1'=>'required',
            'foreskin'=>'required',
            'scrotum'=>'required',
            'dysfunction'=>'required',
            'hypertension'=>'required',
            'anaemia'=>'required',
            'diabetes'=>'required',
            'hiv'=>'required',
            'local'=>'required',
            'antiseptics'=>'required',
            'Systolic'=>'required',
            'diastolic'=>'required',
            'pulse'=>'required',
            'temp'=>'required',
            'rr'=>'required',
            'weight'=>'required',
            'disc'=>'required',
            'adhesions'=>'required',
            'GUD'=>'required',
            'Genital'=>'required',
            'abnormalities'=>'required',
            'Balanitis'=>'required',
            'Surg'=>'required',
            'STI'=>'required',
            'scars'=>'required',
            'Jiggers'=>'required',
            'spec2'=>'required',
            'spec'=>'required',
            'TT'=>'required',
            'good'=>'required',
            'counseled'=>'required',
            'assent'=>'required',
            'exam'=>'required',
            'dtt'=>'required',
            's_time'=>'required',
            'e_time'=>'required',
            'Lign'=>'required',
            'Bupi'=>'required',
            'proce'=>'required',
            'prep'=>'required',
            'siz'=>'required',
            'ring'=>'required',
            'sizz'=>'required',
            'nam'=>'required',
            'cadre'=>'required',
            'assis'=>'required',
            'cadre1'=>'required',
            'events'=>'required',
            'severity2'=>'required',
            'tgiven'=>'required',
            'Systolic2'=>'required',
            'diastolic2'=>'required',
            'Puls'=>'required',
            'rrz'=>'required',
            'listpost'=>'required',
       ]);
       $smcmain = new smcmain;
       $smcmain->sitetype= $request->st;
       $smcmain->NatID = $request->ID;
       $smcmain->SerID= $request->serid ;
       $smcmain->hw_u_heard_smc= implode(",",$request->hw_u_heard_smc);
       $smcmain->careentrypoint = $request->entry;
       $smcmain->sexuallyactive  = $request->sexual;
       $smcmain->testbefore = $request->tested;
       $smcmain->HTCoffered =$request->hct;
       $smcmain->testnow= $request->during;
       $smcmain->testresults = $request->sexual1; 
       $smcmain->incare= $request->care;
       $smcmain->reffered= $request->referred;
       $smcmain->TT1 = $request->TT1;
       $smcmain->TT2= $request->TT2;
       $smcmain->bleeding= $request->disorder;
       $smcmain->ulcers = $request->ulcers;
       $smcmain->urethral = $request->discharge;
       $smcmain->penile_wts= $request->warts;
       $smcmain->pain_on_urn = $request->pain;
       $smcmain->diff_in_foreskin = $request->foreskin;
       $smcmain->swelling_scrotum= $request->scrotum; 
       $smcmain->elec_dysfunction= $request->dysfunction;
       $smcmain->other_specify= $request->spec3;
       $smcmain->hypertension= $request->hypertension;
       $smcmain->anaemia = $request->anaemia;
       $smcmain->diabetes= $request->diabetes;
       $smcmain->HIV_AIDS= $request->hiv;
       $smcmain->other_spec = $request->spec;
       $smcmain->local_anesthetics = $request->local;
       $smcmain->Antiseptics = $request->antiseptics;
       $smcmain->other_medic=$request->spec1;
       $smcmain->Systolic = $request->Systolic;
       $smcmain->Diastolic = $request->diastolic;
       $smcmain->pulse= $request->pulse;
       $smcmain->temp = $request->temp;
       $smcmain->RR = $request->rr;
       $smcmain->weight= $request->weight;
       $smcmain->urethra_d= $request->disc;
       $smcmain->Adhesion= $request->adhesions;
       $smcmain->GUD  = $request->GUD;
       $smcmain->Genital_wts = $request->Genital;
       $smcmain->Anotomical_abns =$request->abnormalities;
       $smcmain->Blanitis = $request->Balanitis;
       $smcmain->Surg_disorders = $request->Surg;
       $smcmain->STI_abns= $request->STI;
       $smcmain->openwound= $request->scars;
       $smcmain->jiggers= $request->Jiggers;
       $smcmain->otherspecif= $request->spec2;
       $smcmain->TTgiven= $request->TT;
       $smcmain->goodhealth = $request->good;
       $smcmain->cc_smc = $request->counseled;
       $smcmain->ic_smc = $request->assent;
       $smcmain->eligible = $request->exam;
       $smcmain->date_of_cmsn= $request->dtt;
       $smcmain->time_started= $request->s_time;
       $smcmain->end_time= $request->e_time;
       $smcmain->lignocaine= $request->Lign;
       $smcmain->bupivicaine= $request->Bupi;
       $smcmain->typeofcircumcision = $request->proce;
       $smcmain->others = $request->othersp;
       $smcmain->prepex= $request->prep;
       $smcmain->size =$request->siz;
       $smcmain->shangring = $request->ring;
       $smcmain->sizze = $request->sizz;
       $smcmain->circumciser= $request->nam;
       $smcmain->cadre= $request->cadre;
       $smcmain->assistant=$request->assis;
       $smcmain->cadr = $request->cadre1;
       $smcmain->events_during_proc = $request->events; 
       $smcmain->severit= $request->severity2;
       $smcmain->Tmtgiven = $request->tgiven;
       $smcmain->systo= $request->Systolic2;
       $smcmain->diasto= $request->diastolic2;
       $smcmain->PULS= $request->Puls;
       $smcmain->R= $request->rrz;
       $smcmain->LPO_medic = $request->listpost;
       $smcmain->save();
       session()->flash('message','Record Inserted Successfully');
        return redirect('/malecircumsion2');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $smcmain=smcmain::find($id);
        return view('more',compact('smcmain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $smcmain=smcmain::find($id);
        return view('smcmain.edit',compact('smcmain'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'ID'=>'required',
            'st'=>'required',
            'sexual'=>'required:smcmains',
            'serid'=>'required',
            'tested'=>'required',
            'hct'=>'required', 
            'during'=>'required',
            'TT1'=>'required', 
            'TT2'=>'required',
            'sexual1'=>'required',
            'care'=>'required',
            'referred'=>'required',
            'disorder'=>'required',
            'ulcers'=>'required',
            'discharge'=>'required',
            'warts'=>'required',
            'pain'=>'required',
            'spec1'=>'required',
            'foreskin'=>'required',
            'scrotum'=>'required',
            'dysfunction'=>'required',
            'hypertension'=>'required',
            'anaemia'=>'required',
            'diabetes'=>'required',
            'hiv'=>'required',
            'local'=>'required',
            'antiseptics'=>'required',
            'Systolic'=>'required',
            'diastolic'=>'required',
            'pulse'=>'required',
            'temp'=>'required',
            'rr'=>'required',
            'weight'=>'required',
            'disc'=>'required',
            'adhesions'=>'required',
            'GUD'=>'required',
            'Genital'=>'required',
            'abnormalities'=>'required',
            'Balanitis'=>'required',
            'Surg'=>'required',
            'STI'=>'required',
            'scars'=>'required',
            'Jiggers'=>'required',
            'spec2'=>'required',
            'spec'=>'required',
            'TT'=>'required',
            'good'=>'required',
            'counseled'=>'required',
            'assent'=>'required',
            'exam'=>'required',
            'dtt'=>'required',
            's_time'=>'required',
            'e_time'=>'required',
            'Lign'=>'required',
            'Bupi'=>'required',
            'proce'=>'required',
            'prep'=>'required',
            'siz'=>'required',
            'ring'=>'required',
            'sizz'=>'required',
            'nam'=>'required',
            'cadre'=>'required',
            'assis'=>'required',
            'cadre1'=>'required',
            'events'=>'required',
            'severity2'=>'required',
            'tgiven'=>'required',
            'Systolic2'=>'required',
            'diastolic2'=>'required',
            'Puls'=>'required',
            'rrz'=>'required',
            'listpost'=>'required',
       ]);
       $smcmain = smcmain::find($id);
       $smcmain->sitetype= $request->st;
       $smcmain->NatID = $request->ID;
       $smcmain->SerID= $request->serid ;
       $smcmain->hw_u_heard_smc= explode(",",$request->hw_u_heard_smc);
       $smcmain->careentrypoint = $request->entry;
       $smcmain->sexuallyactive  = $request->sexual;
       $smcmain->testbefore = $request->tested;
       $smcmain->HTCoffered =$request->hct;
       $smcmain->testnow= $request->during;
       $smcmain->testresults = $request->sexual1; 
       $smcmain->incare= $request->care;
       $smcmain->reffered= $request->referred;
       $smcmain->TT1 = $request->TT1;
       $smcmain->TT2= $request->TT2;
       $smcmain->bleeding= $request->disorder;
       $smcmain->ulcers = $request->ulcers;
       $smcmain->urethral = $request->discharge;
       $smcmain->penile_wts= $request->warts;
       $smcmain->pain_on_urn = $request->pain;
       $smcmain->diff_in_foreskin = $request->foreskin;
       $smcmain->swelling_scrotum= $request->scrotum; 
       $smcmain->elec_dysfunction= $request->dysfunction;
       $smcmain->other_specify= $request->spec3;
       $smcmain->hypertension= $request->hypertension;
       $smcmain->anaemia = $request->anaemia;
       $smcmain->diabetes= $request->diabetes;
       $smcmain->HIV_AIDS= $request->hiv;
       $smcmain->other_spec = $request->spec;
       $smcmain->local_anesthetics = $request->local;
       $smcmain->Antiseptics = $request->antiseptics;
       $smcmain->other_medic=$request->spec1;
       $smcmain->Systolic = $request->Systolic;
       $smcmain->Diastolic = $request->diastolic;
       $smcmain->pulse= $request->pulse;
       $smcmain->temp = $request->temp;
       $smcmain->RR = $request->rr;
       $smcmain->weight= $request->weight;
       $smcmain->urethra_d= $request->disc;
       $smcmain->Adhesion= $request->adhesions;
       $smcmain->GUD  = $request->GUD;
       $smcmain->Genital_wts = $request->Genital;
       $smcmain->Anotomical_abns =$request->abnormalities;
       $smcmain->Blanitis = $request->Balanitis;
       $smcmain->Surg_disorders = $request->Surg;
       $smcmain->STI_abns= $request->STI;
       $smcmain->openwound= $request->scars;
       $smcmain->jiggers= $request->Jiggers;
       $smcmain->otherspecif= $request->spec2;
       $smcmain->TTgiven= $request->TT;
       $smcmain->goodhealth = $request->good;
       $smcmain->cc_smc = $request->counseled;
       $smcmain->ic_smc = $request->assent;
       $smcmain->eligible = $request->exam;
       $smcmain->date_of_cmsn= $request->dtt;
       $smcmain->time_started= $request->s_time;
       $smcmain->end_time= $request->e_time;
       $smcmain->lignocaine= $request->Lign;
       $smcmain->bupivicaine= $request->Bupi;
       $smcmain->typeofcircumcision = $request->proce;
       $smcmain->others = $request->othersp;
       $smcmain->prepex= $request->prep;
       $smcmain->size =$request->siz;
       $smcmain->shangring = $request->ring;
       $smcmain->sizze = $request->sizz;
       $smcmain->circumciser= $request->nam;
       $smcmain->cadre= $request->cadre;
       $smcmain->assistant=$request->assis;
       $smcmain->cadr = $request->cadre1;
       $smcmain->events_during_proc = $request->events; 
       $smcmain->severit= $request->severity2;
       $smcmain->Tmtgiven = $request->tgiven;
       $smcmain->systo= $request->Systolic2;
       $smcmain->diasto= $request->diastolic2;
       $smcmain->PULS= $request->Puls;
       $smcmain->R= $request->rrz;
       $smcmain->LPO_medic = $request->listpost;
       $smcmain->save();
       session()->flash('message','Record Updated Successfully');
        return redirect('/malecircumsion2');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $smcmain=$smcmain::find($id);
        $smcmain->delete();
        session()->flash('message','Deleted Successfully');
        return redirect('malecircumsion2');
    }
}
