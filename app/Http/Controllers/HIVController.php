<?php

namespace Streamline\Http\Controllers;

use Illuminate\Http\Request;
use Streamline\Patient;
use Streamline\PatientCategory;
use Streamline\MaritalStatus;
use Streamline\DrugCategory;
use Streamline\HivCounsellingAndTesting;

class HIVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function menu()
    {
        $patient_id = session()->get('patient_id');
        $episode_id = session()->get('episode_id');
        $patient = Patient::where('id', $patient_id)->first();
        $categories = PatientCategory::pluck("name", "id");
        $marital_statuses = MaritalStatus::pluck("name", "id");
        $drug_categories = DrugCategory::get();

        return view('hiv.menu',compact('patient_id', 'episode_id', 'patient', 'categories', 'marital_statuses','drug_categories'));
    }

    public function hiv_counselling_and_testing()
    {
        $patient_id = session()->get('patient_id');
        $episode_id = session()->get('episode_id');
        $patient = Patient::where('id', $patient_id)->first();
        $categories = PatientCategory::pluck("name", "id");
        $marital_statuses = MaritalStatus::pluck("name", "id");
        $drug_categories = DrugCategory::get();

        return view('hiv.hiv_counselling_and_testing',compact('patient_id', 'episode_id', 'patient', 'categories', 'marital_statuses','drug_categories'));
    }

    public function store_hct_form(Request $request)
    {
        $patient_id = session()->get('patient_id');
        $episode_id = session()->get('episode_id');

        $hiv_counselling_and_testing = new HivCounsellingAndTesting; 
        $hiv_counselling_and_testing->patient_id = $patient_id;
        $hiv_counselling_and_testing->episode_id = $episode_id;       
        $hiv_counselling_and_testing->counselling_date = isset($request->counselling_date) ? Carbon::createFromFormat('d/m/Y', $request->counselling_date)->toDateString() : null;
        $hiv_counselling_and_testing->is_center_static = $request->is_center_static;
        $hiv_counselling_and_testing->registration_number = $request->registration_number;
        $hiv_counselling_and_testing->accompanied_by = $request->accompanied_by;
        $hiv_counselling_and_testing->counselled_as = $request->counselled_as;
        $hiv_counselling_and_testing->approach = $request->approach;
        $hiv_counselling_and_testing->hct_entry_point = $request->hct_entry_point;
        $hiv_counselling_and_testing->ever_tested_for_hiv_before = $request->ever_tested_for_hiv_before;
        $hiv_counselling_and_testing->times_tested_in_last_12_months = $request->times_tested_in_last_12_months;
        $hiv_counselling_and_testing->number_of_sexual_partners_in_last_12_months = $request->number_of_sexual_partners_in_last_12_months;
        $hiv_counselling_and_testing->partner_tested_before = $request->partner_tested_before;
        $hiv_counselling_and_testing->partner_results = $request->partner_results;
        $hiv_counselling_and_testing->confirmatory_lab = $request->confirmatory_lab;
        $hiv_counselling_and_testing->results_received = $request->results_received;
        $hiv_counselling_and_testing->results_received_as_a_couple = $request->results_received_as_a_couple;
        $hiv_counselling_and_testing->is_there_suspision_for_tb = $request->is_there_suspision_for_tb;
        $hiv_counselling_and_testing->has_client_started_cotrimoxazole_prophylaxis = $request->has_client_started_cotrimoxazole_prophylaxis;
        $hiv_counselling_and_testing->has_client_been_linked_to_care = $request->has_client_been_linked_to_care;
        $hiv_counselling_and_testing->where_is_the_client_care = $request->where_is_the_client_care;
        $hiv_counselling_and_testing->cd4_count_results = $request->cd4_count_results;
        $hiv_counselling_and_testing->hiv_recency_test_result = $request->hiv_recency_test_result;
        $hiv_counselling_and_testing->counsellor = $request->counsellor;
        $hiv_counselling_and_testing->save();
        flash("couselling and testing details have been saved")->success();
        
        return redirect('hiv_counselling_and_testing');
    }
    /*
    * appointment follow up form
    */
    public function appointment_follow_up()
    {
        $patient_id = session()->get('patient_id');
        $episode_id = session()->get('episode_id');
        $patient = Patient::where('id', $patient_id)->first();
        $categories = PatientCategory::pluck("name", "id");
        $marital_statuses = MaritalStatus::pluck("name", "id");
        $drug_categories = DrugCategory::get();

        return view('hiv.appointment_follow_up',compact('patient_id', 'episode_id', 'patient', 'categories', 'marital_statuses','drug_categories'));
    }

    public function health_unit_tb_form()
    {
        $patient_id = session()->get('patient_id');
        $episode_id = session()->get('episode_id');
        $patient = Patient::where('id', $patient_id)->first();
        $categories = PatientCategory::pluck("name", "id");
        $marital_statuses = MaritalStatus::pluck("name", "id");
        $drug_categories = DrugCategory::get();
        return view('hiv.health_unit_tuberclosis_form',compact('patient_id', 'episode_id', 'patient', 'categories', 'marital_statuses','drug_categories'));
    }
}
