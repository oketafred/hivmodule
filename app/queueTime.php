<?php

class QueueTime
{
    public function __construct($customers, $tills)
    {
        $this->customers = $customers;
        $this->tills     = $tills;
    }

    public function calculateQueueTime()
    {
        if ($this->tills == 1) {
            return count($this->$customers);
        }
    }
}
