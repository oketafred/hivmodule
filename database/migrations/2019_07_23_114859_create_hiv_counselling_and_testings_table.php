<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHivCounsellingAndTestingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hiv_counselling_and_testings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id');
            $table->integer('episode_id');
            $table->string('counselling_date');
            $table->boolean('is_center_static')->nullable();
            $table->string('registration_number')->nullable();
            $table->string('accompanied_by')->nullable();
            $table->string('counselled_as')->nullable();
            $table->string('approach')->nullable();
            $table->integer('hct_entry_point')->nullable();
            $table->integer('ever_tested_for_hiv_before')->nullable();
            $table->integer('times_tested_in_last_12_months')->nullable();
            $table->integer('number_of_sexual_partners_in_last_12_months')->nullable();
            $table->integer('partner_tested_before')->nullable();
            $table->integer('partner_results')->nullable();
            $table->integer('confirmatory_lab')->nullable();
            $table->integer('results_received')->nullable();
            $table->string('results_received_as_a_couple')->nullable();
            $table->integer('is_there_suspision_for_tb')->nullable();
            $table->integer('has_client_started_cotrimoxazole_prophylaxis')->nullable();
            $table->integer('has_client_been_linked_to_care')->nullable();
            $table->string('where_is_the_client_care')->nullable();
            $table->integer('cd4_count_results')->nullable();
            $table->integer('hiv_recency_test_result')->nullable();
            $table->string('counsellor')->nullable();
            $table->timestamps();

            // for soft deletes to create deleted_at column
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hiv_counselling_and_testings');
    }
}
