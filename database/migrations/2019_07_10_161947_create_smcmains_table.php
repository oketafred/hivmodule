<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmcmainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smcmains', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sitetype');
            $table->string('NatID');
            $table->string('SerID');
            $table->string('hw_u_heard_smc');
            $table->string('careentrypoint');
            $table->string('sexuallyactive');
            $table->string('testbefore');
            $table->string('HTCoffered');
            $table->string('testnow');
            $table->string('testresults');
            $table->string('incare');
            $table->string('reffered');
            $table->date('TT1');
            $table->date('TT2');
            $table->string('bleeding');
            $table->string('ulcers');
            $table->string('urethral');
            $table->string('penile_wts');
            $table->string('pain_on_urn');
            $table->string('diff_in_foreskin');
            $table->string('swelling_scrotum');
            $table->string('elec_dysfunction');
            $table->string('other_specify');
            $table->string('hypertension');
            $table->string('anaemia');
            $table->string('diabetes');
            $table->string('HIV_AIDS');
            $table->string('other_spec');
            $table->string('local_anesthetics');
            $table->string('Antiseptics');
            $table->string('other_medic');
            $table->string('Systolic');
            $table->string('Diastolic');
            $table->string('pulse');
            $table->string('temp');
            $table->string('RR');
            $table->string('weight');
            $table->string('urethra_d');
            $table->string('Adhesion');
            $table->string('GUD');
            $table->string('Genital_wts');
            $table->string('Anotomical_abns');
            $table->string('Blanitis');
            $table->string('Surg_disorders');
            $table->string('STI_abns');
            $table->string('openwound');
            $table->string('jiggers');
            $table->string('otherspecif');
            $table->string('TTgiven');
            $table->string('goodhealth');
            $table->string('cc_smc');
            $table->string('ic_smc');
            $table->string('eligible');
            $table->date('date_of_cmsn');
            $table->time('time_started');
            $table->time('end_time');
            $table->string('lignocaine');
            $table->string('bupivicaine');
            $table->string('typeofcircumcision');
             $table->string('others');
            $table->string('prepex');
            $table->string('size');
            $table->string('shangring');
            $table->string('sizze');
            $table->string('circumciser');
            $table->string('cadre');
            $table->string('assistant');
            $table->string('cadr');
            $table->string('events_during_proc');
            $table->string('severit');
            $table->string('Tmtgiven');
            $table->string('systo');
            $table->string('diasto');
            $table->string('PULS');
            $table->string('R');
            $table->string('LPO_medic');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smcmains');
    }
}
