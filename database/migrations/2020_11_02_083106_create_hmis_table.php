<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unit_tb_number');
            $table->string('hsb_tb_number');
            $table->string('district_tb_number');
            $table->string('contact_phone_number');
            $table->string('relationship_of_contact_person');
            $table->string('is_patient_health_worker');
            $table->string('cadre_of_health_worker');
            $table->string('date_treatment_started');
            $table->string('regime');
            $table->string('type_pf_patient');
            $table->string('specific_regimen');
            $table->string('transfer_in_form');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hmis');
    }
}
